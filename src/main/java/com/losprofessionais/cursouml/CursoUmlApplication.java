// Classe principal //
package com.losprofessionais.cursouml;

//@autor = Starley Cazorla
//@email = stdev@outlook.com.br

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// CommandLineRunner inicia o processo de salvar no banco de dados //
public class CursoUmlApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CursoUmlApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {

	}
}
