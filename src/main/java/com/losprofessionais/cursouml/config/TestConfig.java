package com.losprofessionais.cursouml.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.losprofessionais.cursouml.services.DBService;

// Classe de configuração //
@Configuration
@Profile("test")
public class TestConfig {

	// Implementando DBService //
	@Autowired
	private DBService dbService;

	// bean de configuração //
	@Bean
	public boolean instantiateDataBase() throws ParseException {

		// Buscando classe de serviço //
		dbService.instantiateTestDataBase();

		return true;
	}

}
