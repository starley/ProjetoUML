package com.losprofessionais.cursouml.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.losprofessionais.cursouml.domain.Produto;

// Interface de acesso a produtos //

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
