package com.losprofessionais.cursouml.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.losprofessionais.cursouml.domain.Cliente;
import com.losprofessionais.cursouml.repository.ClienteRepository;
import com.losprofessionais.cursouml.repository.EnderecoRepository;
import com.losprofessionais.cursouml.services.exceptions.ObjectNotFoundException;

@Service // Definindo o cliente como serviço //
public class ClienteService {

	@Autowired
	private ClienteRepository repo;

	@Autowired
	private EnderecoRepository enderecoRepository;

	// Metodo de buscar o cliente //
	public Cliente find(Integer id) {

		Optional<Cliente> obj = repo.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Id não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName()));

	}

	// Inserindo Cliente //
	@Transactional
	public Cliente insert(Cliente obj) {

		obj.setId(null);
		obj = repo.save(obj);
		enderecoRepository.saveAll(obj.getEndereco());
		return obj;

	}

	// Atualizando Cliente //
	public Cliente update(Cliente obj) {

		// Buscando cliente //
		Cliente newObj = find(obj.getId());
		updateData(newObj, obj);

		// Salavando atualização //
		return repo.save(newObj);

	}

	// Metodo para exluir //
	public void delete(Integer id) {

		// Buscando id do cliente //
		find(id);

		// Tentando realizar a exclusão //
		try {
			repo.deleteById(id);
		} catch (DataIntegrityViolationException dv) {

			// Tratando erro //
			throw new DataIntegrityViolationException(
					"Não é possível excluir o cliente pois existe outras tabelas vinculadas! Erro: " + dv);

		}

	}

	// Metodo auxiliar para atualização de clientes //
	private void updateData(Cliente newObj, Cliente obj) {

		newObj.setNome(obj.getNome());
		newObj.setEmail(obj.getEmail());

	}

}
