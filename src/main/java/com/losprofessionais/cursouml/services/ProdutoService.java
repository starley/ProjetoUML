package com.losprofessionais.cursouml.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.losprofessionais.cursouml.domain.Produto;
import com.losprofessionais.cursouml.repository.ProdutoRepository;
import com.losprofessionais.cursouml.services.exceptions.ObjectNotFoundException;

@Service // Definindo a classe como um serviço //
public class ProdutoService {

	@Autowired
	private ProdutoRepository repo;

//	@Autowired
//	private CategoriaRepository categoriaRepository;

	// Buscando o produto pelo id //
	public Produto find(Integer id) {

		Optional<Produto> obj = repo.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Id não foi encontrado :" + id + " Tipo: " + Produto.class.getName()));

	}

}
