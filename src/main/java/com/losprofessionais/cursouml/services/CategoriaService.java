package com.losprofessionais.cursouml.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.losprofessionais.cursouml.domain.Categoria;
import com.losprofessionais.cursouml.repository.CategoriaRepository;
import com.losprofessionais.cursouml.services.exceptions.ObjectNotFoundException;

// Classe responsavel por oferecer serviços ao conteudo rest //

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository repo;

	// Buscando categoria por Id //
	public Categoria find(Integer id) {

		Optional<Categoria> obj = repo.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Id não encontrado! Id: " + id + ", Tipo: " + Categoria.class.getName()));

	}

	// Inserindo Categoria //
	public Categoria insert(Categoria obj) {

		obj.setId(null);

		return repo.save(obj);

	}

	// Atualizando Categoria //
	public Categoria update(Categoria obj) {

		// Buscando categoria para atualização //
		Categoria newObj = find(obj.getId());
		updateData(newObj, obj);

		return repo.save(newObj);
	}

	// Metodo para atualizar categoria //
	private void updateData(Categoria newObj, Categoria obj) {

		// Atualizando nome //
		newObj.setNome(obj.getNome());

	}

}
