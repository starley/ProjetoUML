package com.losprofessionais.cursouml.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.losprofessionais.cursouml.domain.ItemPedido;
import com.losprofessionais.cursouml.domain.PagamentoComBoleto;
import com.losprofessionais.cursouml.domain.Pedido;
import com.losprofessionais.cursouml.domain.enuns.EstadoPagamento;
import com.losprofessionais.cursouml.repository.PagamentoRepository;
import com.losprofessionais.cursouml.repository.PedidoRepository;
import com.losprofessionais.cursouml.services.exceptions.ObjectNotFoundException;

@Service // Definindo a classe como um serviço //
public class PedidoService {

	@Autowired
	private PedidoRepository repo;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private BoletoService boletoService;

	@Autowired
	private PagamentoRepository pagamentoRepository;

	@Autowired
	private ProdutoService produtoService;

	// Buscando pedido //
	public Pedido find(Integer id) {

		Optional<Pedido> obj = repo.findById(id);

		// Tratamento de erro //
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Pedido não encontrado. ID = " + id + ". Tipo: " + Pedido.class.getName()));
	}

	// Inserindo pedido //
	public Pedido insert(Pedido obj) {

		obj.setId(null);
		obj.setInstante(new Date());
		obj.setCliente(clienteService.find(obj.getCliente().getId()));
		obj.getPagamento().setEstado(EstadoPagamento.PENDENTE);
		obj.getPagamento().setPedido(obj);
		if (obj.getPagamento() instanceof PagamentoComBoleto) {

			PagamentoComBoleto pagto = (PagamentoComBoleto) obj.getPagamento();
			boletoService.preencherPagamentoComBoleto(pagto, obj.getInstante());

		}
		// Salvando pagamento no pedido //
		obj = repo.save(obj);
		pagamentoRepository.save(obj.getPagamento());

		for (ItemPedido ip : obj.getItens()) {
			ip.setDesconto(0.0);
			ip.setProduto(produtoService.find(ip.getProduto().getId()));
			ip.setPreco(ip.getProduto().getPreco());
			ip.setPedido(obj);
		}

		return obj;
	}

}
