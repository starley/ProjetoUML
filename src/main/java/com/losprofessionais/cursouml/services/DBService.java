package com.losprofessionais.cursouml.services;

import java.text.ParseException;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.losprofessionais.cursouml.domain.Categoria;
import com.losprofessionais.cursouml.domain.Cidade;
import com.losprofessionais.cursouml.domain.Cliente;
import com.losprofessionais.cursouml.domain.Endereco;
import com.losprofessionais.cursouml.domain.Estado;
import com.losprofessionais.cursouml.domain.Produto;
import com.losprofessionais.cursouml.domain.enuns.TipoCliente;
import com.losprofessionais.cursouml.repository.CategoriaRepository;
import com.losprofessionais.cursouml.repository.CidadeRepository;
import com.losprofessionais.cursouml.repository.ClienteRepository;
import com.losprofessionais.cursouml.repository.EnderecoRepository;
import com.losprofessionais.cursouml.repository.EstadoRepository;
import com.losprofessionais.cursouml.repository.ProdutoRepository;

// Classe de serviço para popular banco de dados //

@Service
public class DBService {

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private EnderecoRepository enderecoRepository;

	@Autowired
	private EstadoRepository estadoRepository;

	@Autowired
	private CidadeRepository cidadeRepository;

	public void instantiateTestDataBase() throws ParseException {

		// Criando e listando categorias //
		Categoria cat1 = new Categoria(1, "Informatica");
		Categoria cat2 = new Categoria(2, "Escritorio");

		categoriaRepository.saveAll(Arrays.asList(cat1, cat2));

		Produto p1 = new Produto(null, "Computador", 2000.00);
		Produto p2 = new Produto(null, "Impressora", 900.00);
		Produto p3 = new Produto(null, "Mesa", 200.00);

		// Associando produtos com as categorais //
		cat1.getProdutos().addAll(Arrays.asList(p1, p2));
		cat2.getProdutos().addAll(Arrays.asList(p3));

		p1.getCategorias().addAll(Arrays.asList(cat1));
		p2.getCategorias().addAll(Arrays.asList(cat1, cat2));
		p3.getCategorias().addAll(Arrays.asList(cat1));

		categoriaRepository.saveAll(Arrays.asList(cat1, cat2));

		produtoRepository.saveAll(Arrays.asList(p1, p2, p3));

		// Cidades e Estados //
		Estado est1 = new Estado(null, "Minas Gerais");
		Estado est2 = new Estado(null, "São Paulo");

		Cidade c1 = new Cidade(null, "Uberlandia", est1);
		Cidade c2 = new Cidade(null, "São Paulo", est2);
		Cidade c3 = new Cidade(null, "Campinas", est2);

		est1.getCidades().addAll(Arrays.asList(c1));
		est2.getCidades().addAll(Arrays.asList(c2, c3));

		// Salvando os estados e cidades //
		estadoRepository.saveAll(Arrays.asList(est1, est2));
		cidadeRepository.saveAll(Arrays.asList(c1, c2, c3));

		Cliente cl1 = new Cliente(null, "Starley", "email@mail", "73672564168", TipoCliente.PESSOAFISICA);
		Cliente cl2 = new Cliente(null, "Eliana", "ali@gmail.com", "15028994000121", TipoCliente.PESSOAJURIDICA);

		clienteRepository.saveAll(Arrays.asList(cl1, cl2));

		Endereco end1 = new Endereco(null, "Rua 8", "SN", "Qd-L1 Lt6", "Vila Pedroso", "74770-000", c1, cl1);
		Endereco end2 = new Endereco(null, "Rua 8", "SN", "Qd-L1 Lt6", "Vila Pedroso", "74770-000", c2, cl2);

		enderecoRepository.saveAll(Arrays.asList(end1, end2));

	}

}
