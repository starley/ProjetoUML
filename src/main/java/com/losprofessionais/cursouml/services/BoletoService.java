package com.losprofessionais.cursouml.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.losprofessionais.cursouml.domain.PagamentoComBoleto;

@Service
public class BoletoService {

	// Classe para calcular data de vencimento com boleto com data definida em 7
	// dias //
	public void preencherPagamentoComBoleto(PagamentoComBoleto pagto, Date instanteDoPedido) {

		// Instanciando um calendario //
		Calendar cal = Calendar.getInstance();
		// setando instante no pedido //
		cal.setTime(instanteDoPedido);
		// Adiconando 7 dias //
		cal.add(Calendar.DAY_OF_MONTH, 7);
		// Atribuindo a data para pagamento //
		pagto.setDataPagamento(cal.getTime());

	}

}
