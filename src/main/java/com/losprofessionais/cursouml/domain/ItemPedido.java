package com.losprofessionais.cursouml.domain;

import java.io.Serializable;

import javax.persistence.EmbeddedId;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ItemPedido implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@EmbeddedId // Id embutido em um tipo auxiliar
	private ItemPedidoPK id = new ItemPedidoPK();

	private Double desconto;
	private int quantidade;
	private Double preco;

	// Construtor vazio //
	public ItemPedido() {

	}

	// Construtor com argumentos //
	public ItemPedido(ItemPedidoPK id, Double desconto, int quantidade, Double preco) {
		super();
		this.id = id;
		this.desconto = desconto;
		this.quantidade = quantidade;
		this.preco = preco;
	}

	// Metodo para retornar o valor total de um item de pedido //
	public Double getSubTotal() {

		// Retornando ao metodo //
		return (preco - desconto) * quantidade;

	}

	// Criando dois metodos de retorno para acesso fora da classe //
	@JsonIgnore
	public Pedido getPedido() {
		return id.getPedido();
	}

	public void setPedido(Pedido pedido) {
		id.setPedido(pedido);
	}

	public Produto getProduto() {
		return id.getProduto();
	}

	public void setProduto(Produto produto) {
		id.setProduto(produto);
	}

	public ItemPedidoPK getId() {
		return id;
	}

	public void setId(ItemPedidoPK id) {
		this.id = id;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

}
