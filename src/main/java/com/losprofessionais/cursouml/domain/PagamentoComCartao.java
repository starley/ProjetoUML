package com.losprofessionais.cursouml.domain;

import com.losprofessionais.cursouml.domain.enuns.EstadoPagamento;

public class PagamentoComCartao extends Pagamento {

	private static final long serialVersionUID = 1L;

	private Integer numeroDeParcelas;

	// Construtor vazio //
	public PagamentoComCartao() {

	}

	// Construtor com argumentos //
	public PagamentoComCartao(Integer id, EstadoPagamento estado, Pedido pedido, Integer numeroDeParcelas) {
		super(id, estado, pedido);
		this.numeroDeParcelas = numeroDeParcelas;
	}

	public Integer getParcelas() {
		return numeroDeParcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.numeroDeParcelas = parcelas;
	}

}
