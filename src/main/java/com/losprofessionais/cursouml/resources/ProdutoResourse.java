package com.losprofessionais.cursouml.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.losprofessionais.cursouml.domain.Produto;
import com.losprofessionais.cursouml.services.ProdutoService;

@RestController // Definindo a classe como controladora //
@RequestMapping(value = "/produto")
public class ProdutoResourse {

	@Autowired
	private ProdutoService service;

	// Metodo para encontrar o produto //
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id){
		
		Produto obj = service.find(id);
		
		// Retornando a resposta do endpoint //
		return ResponseEntity.ok().body(obj);
		
	}

}
