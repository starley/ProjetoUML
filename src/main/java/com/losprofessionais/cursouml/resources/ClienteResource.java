package com.losprofessionais.cursouml.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.losprofessionais.cursouml.domain.Cliente;
import com.losprofessionais.cursouml.services.ClienteService;

@RestController // Definindo classe como rest //
@RequestMapping(value = "/cliente") // Nome do endpoit //
public class ClienteResource {

	@Autowired
	private ClienteService service;

	// Criando a resposta //
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {

		Cliente obj = service.find(id);

		// Retornando a resposta do endpoint //
		return ResponseEntity.ok().body(obj);
	}

}
