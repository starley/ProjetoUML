package com.losprofessionais.cursouml.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.losprofessionais.cursouml.domain.Categoria;
import com.losprofessionais.cursouml.services.CategoriaService;

// Implementando classe categoria Resource //
@RestController
@RequestMapping(value = "/categoria") // Deve ser entre aspas o caminho //
public class CategoriaResource {
	
	@Autowired
	private CategoriaService service;

	// Metodo de pegar //
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {

		// pegando o id digitado //
		Categoria obj = service.find(id);
		

		// Retornando respota //
		return ResponseEntity.ok().body(obj);
	}
	
	

}
